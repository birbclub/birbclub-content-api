package main

import (
	"birbclub-content-api/internal/server"
	"os"
)

func getOptions() *server.Options {
	return &server.Options{
		Endpoint: ":8080",
		Location: getLocation(),
	}
}

func getLocation() string {
	serverHost := os.Getenv("BIRBCLUB_CONTENT_API_LOCATION")

	if serverHost == "" {
		return "/data"
	}

	return serverHost
}
