package main

import (
	"birbclub-content-api/internal/builtin/server"
)

func main() {
	server.New(getOptions()).Serve()
}
