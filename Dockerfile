FROM golang AS build-env

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY cmd/ ./cmd/
COPY internal/ ./internal/

RUN CGO_ENABLED=0 go build -o /birbclub-content-api birbclub-content-api/cmd/main

FROM scratch

WORKDIR /
COPY --from=build-env /birbclub-content-api /birbclub-content-api

EXPOSE 8080

ENTRYPOINT ["/birbclub-content-api"]
