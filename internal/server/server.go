package server

type Options struct {
	Endpoint string
	Location string
}

type Interface interface {
	Serve()
	Options() *Options
}
