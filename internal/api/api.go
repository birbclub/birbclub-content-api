package api

import (
	"birbclub-content-api/internal/content_provider"
	"birbclub-content-api/internal/server"
)

type Options struct {
	Server          server.Interface
	ContentProvider content_provider.Interface
}
