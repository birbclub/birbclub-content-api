package content_provider

import (
	"birbclub-content-api/internal/content_provider"
	"errors"
	"fmt"
	"os"
	"path"
	"sync"
)

type ContentProvider struct {
	location   string
	cache      []cacheEntry
	cacheMutex sync.Mutex
}

type cacheEntry struct {
	name string
	path string
}

func New(location string) content_provider.Interface {
	return &ContentProvider{
		location: location,
		cache:    make([]cacheEntry, 0),
	}
}

func (provider *ContentProvider) List() ([]string, error) {
	if len(provider.cache) == 0 {
		if err := provider.InvalidateCache(); err != nil {
			return nil, err
		}
	}

	names := make([]string, len(provider.cache))

	for idx, entry := range provider.cache {
		names[idx] = entry.name
	}

	return names, nil
}

func (provider *ContentProvider) Get(name string) (string, error) {
	for _, cacheEntry := range provider.cache {
		if cacheEntry.name == name {
			return cacheEntry.path, nil
		}
	}

	return "", errors.New(fmt.Sprintf("no content for %s", name))
}

func (provider *ContentProvider) InvalidateCache() error {
	provider.cacheMutex.Lock()
	defer provider.cacheMutex.Unlock()

	entries, err := os.ReadDir(provider.location)

	if err != nil {
		return err
	}

	cache := make([]cacheEntry, 0)

	for _, entry := range entries {
		if entry.IsDir() {
			continue
		}

		cache = append(cache, cacheEntry{
			name: entry.Name(),
			path: path.Join(provider.location, entry.Name()),
		})
	}

	provider.cache = cache

	return nil
}
