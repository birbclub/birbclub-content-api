package server

import (
	"birbclub-content-api/internal/api"
	"birbclub-content-api/internal/builtin/api_v1"
	"birbclub-content-api/internal/builtin/content_provider"
	"birbclub-content-api/internal/server"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

type Server struct {
	options *server.Options
	echo    *echo.Echo
}

func New(options *server.Options) server.Interface {
	return &Server{
		options: options,
		echo:    echo.New(),
	}
}

func (s *Server) Serve() {
	s.echo.Use(middleware.Logger())
	s.echo.Use(middleware.Recover())
	s.echo.Use(middleware.CORS())

	apiOptions := &api.Options{
		Server:          s,
		ContentProvider: content_provider.New(s.options.Location),
	}

	s.echo.GET("/ping", func(ctx echo.Context) error { return ctx.String(200, "pong") })

	// v1 api
	apiv1 := api_v1.New(apiOptions)
	s.echo.GET("/v1/content", apiv1.GetAll)
	s.echo.GET("/v1/content/:name", apiv1.GetByName)
	s.echo.GET("/v1/content/reset", apiv1.ResetCache)

	s.echo.Logger.Fatal(s.echo.Start(s.options.Endpoint))
}

func (s *Server) Options() *server.Options {
	return s.options
}
