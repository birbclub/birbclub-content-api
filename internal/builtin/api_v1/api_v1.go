package api_v1

import (
	"birbclub-content-api/internal/api"
	"github.com/labstack/echo/v4"
)

type Api struct {
	options *api.Options
}

type apiErrorResponse struct {
	Message string `json:"message"`
}

type getAllResponse struct {
	Content []string `json:"content"`
}

func New(options *api.Options) *Api {
	return &Api{
		options: options,
	}
}

func (api *Api) GetAll(ctx echo.Context) error {
	names, err := api.options.ContentProvider.List()

	if err != nil {
		return ctx.JSON(400, apiErrorResponse{Message: err.Error()})
	}

	return ctx.JSON(200, getAllResponse{Content: names})
}

func (api *Api) GetByName(ctx echo.Context) error {
	name := ctx.Param("name")

	path, err := api.options.ContentProvider.Get(name)

	if err != nil {
		return ctx.JSON(400, apiErrorResponse{Message: err.Error()})
	}

	return ctx.File(path)
}

func (api *Api) ResetCache(ctx echo.Context) error {
	err := api.options.ContentProvider.InvalidateCache()

	if err != nil {
		return ctx.JSON(400, apiErrorResponse{Message: err.Error()})
	}

	return ctx.NoContent(200)
}
