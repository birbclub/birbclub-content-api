package content_provider

type Interface interface {
	List() ([]string, error)
	Get(name string) (string, error)
	InvalidateCache() error
}
